.. index::
   pair: Women of the Wall ; 2023-06

.. _wow_2023_06:

=========================================================================================
2023-06-1 🌱Zoom Roch Hodech tammouz femmes du mur mardi 6h du matin (courage!) 🕊🌎🕯
=========================================================================================

- https://rabbinchinsky.fr/2023/06/16/%f0%9f%8c%b1zoom-roch-hodech-femmes-du-mur-mardi-6h-du-matin-courage-%f0%9f%95%8a%f0%9f%8c%8e%f0%9f%95%af/
- https://poursurmelin.files.wordpress.com/2023/06/sidour-roch-hodech-2023-06-16.pdf
- https://framaforms.org/roch-hodech-avec-les-femmes-du-mur-1674338190


Bonjour à toutes et tous,

**Nous voulons affirmer notre soutien à la démocratie et à l’égalité
femme-hommes ainsi qu’à toutes les personnes qui défendent ces valeurs**.

En Israël, **les Femmes du Mur sont à la pointe de ce combat**.

Nous avons affirmé notre soutien et pris contact directement avec elles
mardi dernier, nous poursuivons notre engagement.

Rendez-vous sur notre lien Zoom des Femmes du mur mardi prochain,
au programme:

- nous suivrons leur office, je vous commenterai les événements
- nous bouclerons le mois passé et ouvrirons le mois à venir, bienvenue dans Tammouz !
- nous étudierons un petit texte concernant la place des femmes dans le
  judaïsme

Parlez-en à vos ami.es, encouragez-les à suivre l’actualité des Femmes
du mur sur leur site et leurs réseaux sociaux, sur le site de JEM et nos
réseau sociaux, sur mon site rabbinchinsky.fr pour avoir les infos et
les téléchargements.

J’ouvrirai le zoom à 6:55 sur le lien que vous recevrez en complétant ce
formulaire (si vous avez déjà complété le formulaire, vous venez de
recevoir un email avec le lien zoom)

ET RECEVOIR LE LIEN DE CONNEXION AU ZOOM DES FEMMES DU MUR MARDI 6h DU MATIN
(l’horaire matinal est notre courage à nous!) <https://framaforms.org/roch-hodech-avec-les-femmes-du-mur-1674338190>`_

Vous pouvez télécharger et imprimer **le sidour de roch Hodech** que j’ai
retravaillé pour vous, ici LIEN POUR LE `SIDOUR ROCH HODECH <https://poursurmelin.files.wordpress.com/2023/06/sidour-roch-hodech-2023-06-16.pdf>`_
