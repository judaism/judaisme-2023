
.. _hanukka_2023_12_08:

=======================================================================================================
2023-12-08 🕎 **Chabat Hanouka – des chants de Hanouka aux chants de la chorale Zimrat ya!**
=======================================================================================================

- https://rabbinchinsky.fr/2023/12/08/%f0%9f%95%8echabat-hanouka-des-chants-de-hanouka-aux-chants-de-la-chorale-zimrat-ya/

Bonjour à toutes et tous.

Ce soir allumage de la deuxième bougie de Hanouka et allumage des bougies
de chabat.

- L’office aura lieu à Pelleport à 18h45.
- Demain office à 10h30, office Kesher, préparez-vous à participer.
- Commentaire de la paracha à 13h et chorale à 14h.
- Et pour finir en beauté, allumage des bougies demain soir, accueil 17h
  havdala et allumage à 18h, amenez votre Hanoukia pour allumer ensemble !

Hag OURIM SaméaH! חג אורים שמח et chabbat chalom שבת שלום

L’allumage officiel des bougies de Hanouka
=============================================

En une phrase, l’allumage officiel des bougies de Hanouka consiste dans
le fait:

- d'allumer,
- entre la tombée de la nuit et la fin de la circulation piétonne
  dans les rues,
- des bougies multicolores ou pas,
- au moins une bougie chaque soir et selon l’opinion acceptée de Hillel
  une bougie puis deux puis trois etc.,
- placées près de la fenêtre ou près de la porte en face de la mézouza,
- dans un bougeoir spécial ou pas,
- pendant 8 jours, à partir de ce soir,
- en prononçant les deux bénédictions traditionnelles,
- et à passer 30 minutes joyeuses près des bougies,
- qu’on soit un homme ou une femme.


Les deux bénédictions
=====================

Les deux bénédictions sont:

- **BarouH ata adonaï élohénou mélèH haolam acher kidéchanou bémitsvotav vétsivanou léhadlik ner chel Hanouka,**
- **BarouH ata adonaï élohénou mélèH haolam chéassa nissim laavoténou bayamim hahem bazéman hazé**,

On ajoute le premier soir :

- **BarouH ata adonaï élohénou mélèH haolam cheHéyanou vékiyémanou véhiguianou lazéman hazé.**

Pour alimenter les discussions autour des bougies
====================================================

Pour alimenter les discussions autour des bougies, les chants, les jeux,
voici deux éléments exclusifs de cette année, ainsi que toutes les
ressources des années précédentes:

Nouveauté
--------------

Vidéo Hanouka, midrach, antisémitisme et écoute mutuelle

Les essentiels
-----------------

Télécharger

- `la feuille de chant en français, hébreu et translittération ici. Chants de Hanouka  <https://poursurmelin.files.wordpress.com/2013/12/chants-de-hanouka-pour-surmelin.pdf>`_
- `Chants complémentaires de Hanouka <https://poursurmelin.files.wordpress.com/2018/11/chants-complc3a9mentaires-de-hanouka.docx>`_

Vidéos
---------

- `Voir des vidéos sur les chants pour les apprendre ici <https://youtube.com/playlist?list=PLnHlXjFx9rOSME-6R48c5YH08Tr43lkiR>`_.
- `Vidéo d’accompagnement des chants de Hanouka et des chants complémentaires <https://youtube.com/playlist?list=PLnHlXjFx9rOSME-6R48c5YH08Tr43lkiR>`_
- `Vidéo Paracha de la semaine et Hanouka ici <https://www.youtube.com/watch?v=ORev_TWE6eg>`_,
- `Vidéos Boker Tov spécial Hanouka ici <https://www.youtube.com/playlist?list=PLnHlXjFx9rOSI2tIzKbfGgaQdOLL1itdp>`_
- `Vidéos avec des activités pour Hanouka ici (Hanouka mode d’emploi) <https://www.youtube.com/playlist?list=PLnHlXjFx9rOSME-6R48c5YH08Tr43lkiR>`_

Des chants de Hanouka avec les paroles en hébreu dans les sous-titres

- `vidéo 1 <https://www.youtube.com/watch?v=pg3Be6doSCU>`_,
- `vidéo 2 <http://youtube https://www.youtube.com/watch?v=c_MUTab2IyM>`_,
- `vidéo 3 <http://youtube https://www.youtube.com/watch?v=2TDPLFut7V0>`_

Textes
----------

Deux textes sur les origines de la fête :

- `Hanouka, fête de la lumière universelle, <https://libertejuive.wordpress.com/2015/11/12/hanouka-universel/>`_
- et `Hanouka, fête historique et légendaire <https://libertejuive.wordpress.com/2015/11/26/hanouka-historique/>`_

Un petit texte de réflexion

- `Célébrons les miracles de demain <https://poursurmelin.wordpress.com/2014/11/10/miracles-hanouka/>`_,
- un article: `Hanouka, Hareng, Messie et Liberté <https://rabbinchinsky.fr/2019/12/13/appel-hanouka/>`_

Une étude pour réfléchir à notre façon de faire l’allumage:

- `Texte pour embellir l’allumage <https://libertejuive.wordpress.com/2012/12/07/allumage-hanouka/>`_

Des jeux pédagogiques autour des bougies, disponible sur ce `lien <https://rabbinchinsky.fr/2018/12/03/activites-hanouka/>`_.
