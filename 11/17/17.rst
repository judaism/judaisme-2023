
.. _judaisme_2023_11_17:

=======================================================================================================
2023-11-17 💚Chabat chalom, et le cercle des familles endeuillées palestiniennes et israéliennes
=======================================================================================================


- https://rabbinchinsky.fr/2023/11/17/%f0%9f%92%9achabat-chalom-et-le-cercle-des-familles-endeuillees-palestiniennes-et-israeliennes/

Programme de ce chabbat: Ce soir, BM avec le Rabbin Boissière, les offices
ont tous lieu à Surmelin.

💚❤️Youval RaHamim était à JEM-EST ce chabbat
=================================================

- https://www.youtube.com/watch?v=DhumLgptUf4


L’humanité commence par l’écoute
=================================

- https://did.li/zFDTY

« Cultivons notre humanité, rencontre avec les familles endeuillées
israéliennes et palestiniennes, lien zoom: https://did.li/zFDTY.

Les intervenant.es: Maoz Ynon, qui a perdu ses deux parents le 7 octobre
à Otef Aza, Dr Moussa, qui a perdu de nombreux membres de sa famille
à Gaza, Ido Argaman, fils endeuillé et membre du Kiboutz Miflassim à Otef Aza,
Bassam Aramin, père endeuillé et Robbie Dalmin, mère endeuillée.

L’humanité commence par l’écoute.

Cela fait déjà presque 40 jours que nous écoutons la souffrance et
l’état de choc, les cris de la perte, les histoires d’héroïsme et les
sirènes et le son des explosions.

Nous vous invitons à élargir la portée de votre écoute, à écouter Maoz,
Moussa, Ido, Bassam et Robbie, israélien.nes et palestinien.nes,
tous endeuillés, tous en souffrance, tous porteurs d’apaisement et
convaincus qu’autre chose est possible.

Venez écouter avec le coeur. »
