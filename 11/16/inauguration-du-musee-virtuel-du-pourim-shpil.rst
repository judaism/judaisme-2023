.. index::
   pair: Pourim shpil; 2023-11-16


.. _pourim_shpil_2023_11_16:

=======================================================================================================
2023-11-16 **Inauguration du Musée virtuel du Pourim shpil**
=======================================================================================================


- https://www.helloasso.com/associations/collectif-pourim-shpil/evenements/inauguration-du-musee-virtuel-du-pourim-shpil


Pour fêter l'inauguration du Musée virtuel du Pourim shpil, le Collectif
Pourim Shpil est heureux de vous le présenter - avant sa mise en ligne publique -
le dimanche 19 novembre 2023 à 15h15 à l’Espace Rachi-Guy de Rothschild
39 Rue Broca, 75005 Paris.

L'après-midi sera animée par Eden Gerber, clarinettiste, Sylvain Dubert,
guitariste, Miriam Camerini, chanteuse présenteront un spectacle original
en yiddish, judéo-espagnol et français. Un goûter clôturera l’après-midi.

Entrée dès 15h15.

Annonce AACE
================

- https://www.aacce.fr/2023/11/evenement.html

L'AACCE membre du Collectif Pourim Shpil vous invite à venir nombreux à
l'Inauguration du Musée virtuel du Pourim shpil Dans le cadre Journées
Européennes de la Culture Juive Réservation obligatoire sur HelloAsso en
cliquant ICI

.. figure:: images/affiche.jpg
