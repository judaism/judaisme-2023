.. index::
   pair: LesBubbeyMayse; Klezmer
   ! klezmer

.. _klezmer_2023_11:

=======================================================================================================
**Festival JAZZ’N’KLEZMER – 21e ÉDITION du 8 au 19 Novembre 2023**
=======================================================================================================

- https://www.iemj.org/festival-jazznklezmer-21e-edition/
- https://jazznklezmer.fr/


:download:`Télécharger le programme au format PDF <pdf/Programme-FJNK-WEB.pdf>`

Du 8 au 19 Novembre 2023
==============================

Après avoir voyagé à travers le monde entier, cette 21e édition sera
Jazz AND Klezmer !

Avec une oreille toujours attentive à la scène Jazz Israélienne, le chef
d’orchestre de cette 21e édition sera ce brillant précurseur du piano
klezmer, Denis Cuniot, qui dans ce projet en Duos avec, pour et par
Nano Peylet, fait preuve de versatilité très free et d’un feeling si émouvant.

Côté Klezmer, nous allons nous égarer en « Klezmerie »

Le Israeli Klezmer Orchestra, nous fera découvrir toute sa vivacité et
sa modernité pour notre enchantement.

**Les Bubbey Mayse, qui sont la découverte du festival Jazz’N’Klezmer**,
joueront de finesse tout en voix et en acoustique.

Enfin, de l’autre côté de l’Atlantique, le groupe Golem Rock, n’était
pas venu en France depuis 15 ans.

La soirée se clôturera avec DJ Sharouh s’intéresse aux différentes formes
de syncrétisme musical (arabe, judéo-arabe; berbère, mizrahi…) **ainsi
qu’au rôle des femmes dans cet héritage**.

Une édition qui fait un petit tour dans la tradition pour mieux cavaler
vers le futur.


|LesBubbeyMayse| **Mercredi 8 novembre 2023 à 19h00–20h30 LES BUBBEY MAYSE** À GRENOBLE, Réservations au 04 76 50 63 46
============================================================================================================================

- https://compagniezadjo.com/agenda/
- https://jazznklezmer.fr/evenement/les-bubbey-mayse-a-grenoble/
- https://invidious.fdn.fr/channel/UCKpZCRjsoMKFc15Va-VZMxg
- https://www.youtube.com/@lesbubbeymayse9336/videos
- https://bubbeymayse.wixsite.com/bubbey2


**Concert de sortie d’Album édité par l’IEMJ**

Auditorium Blum Weizman, Espace des Cultures juives, 4 bis rue des bains,
38000 Grenoble, Réservations au 04 76 50 63 46


.. figure:: images/a_grenoble.png
   :align: center


.. figure:: images/les_bubbey_mayse_2023_11_08.png
   :align: center


.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.717361867427827%2C45.18543409502039%2C5.720902383327484%2C45.18697101826111&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small>
   <a href="https://www.openstreetmap.org/#map=19/45.18620/5.71913">Afficher une carte plus grande</a></small>


Pour réserver
------------------

- https://www.billetweb.fr/shop.php?event=les-bubbey-mayse-grenoble
- Réservations au 04 76 50 63 46


Le 3e album **Le petit matin du grand soir**
-----------------------------------------------

- https://invidious.fdn.fr/watch?v=2YEvGnZmyhA&ab_channel=Lesbubbeymayse (Les Bubbey Mayse/ Spectacle Le Nouveau Sher)
- https://www.helloasso.com/associations/compagnie-zadjo/boutiques/nouvel-album-des-bubbey-mayse-en-prevente
- https://bubbeymayse.wixsite.com/bubbey2

En pré-achetant notre nouvel album, vous nous soutenez fortement et
vous aurez ainsi l'occasion de recevoir "Le petit matin du grand soir"
dès sa sortie !!

Et en plus, vous avez le choix entre une livraison à domicile ou un
retrait gratuit sur Rennes, Lille, Noirmoutier ou sur une de nos dates
de concert !

Pour infos, les dons que vous verserez à l'association Zadjo seront
entièrement reversés aux Bubbey Mayse.

Merci d'avance à vous !


.. figure:: images/album_3_le_petit_matin_du_grand_soir.png

   https://bubbeymayse.wixsite.com/bubbey2



.. figure:: images/les_3_albums.png
   :align: center

   https://www.helloasso.com/associations/compagnie-zadjo/boutiques/nouvel-album-des-bubbey-mayse-en-prevente



.. figure:: images/les_4_musiciennes.png

   https://bubbeymayse.wixsite.com/bubbey2


- Elsa Signorile, Clarinette – Chant, Musicienne 06 71 87 90 32 contact@compagniezadjo.com
- Morgane Labbe, Accordéon – Chant
- Margaux Liénard, Violon – Chœurs
- Juliette Divry, Violoncelle – Chœurs


**LE PETIT MATIN DU GRAND SOIR**
------------------------------------

- https://bubbeymayse.wixsite.com/bubbey2
- https://www.iemj.org/les-bubbey-mayse-le-petit-matin-du-grand-soir/

.. figure:: images/le_petit_matin_du_grand_soir.png

Les Bubbey Mayse,

**Quartet musique klezmer et yiddish**

Les Bubbey Mayse, c’est **la puissance de quatre femmes qui s’exprime**.

**C’est une musique engagée, une écriture assumée, menée par la force du
collectif**.

Dans leurs instruments, **on entend les cris des histoires qui remuent**,
on sent **planer le pas de la colère**, on devine des racines profondes
avec des envies d’horizon, on **rêve de se blottir dans une danse
jusqu’au petit matin…**

Et dans leurs quatre voix, accordées au cordeau, on plonge dans un univers
entier et épidermique.

**Celui de quatre femmes qui se réunissent autour de l’amour de la musique
klezmer et de la langue yiddish, et qui y puisent l’élan de réinventer
des lendemains qui chantent**.


Avec Les musiciennes :

- Elsa Signorile : clarinette, chant ;
- Juliette Divry : violoncelle, chant ;
- Margaux Liénard : violon, chant ;
- Morgane Labbe : accordéon, chant


Historique
-------------

- https://www.iemj.org/les-bubbey-mayse-le-petit-matin-du-grand-soir/


.. figure:: images/en_une_iemj.png

   https://www.iemj.org/


Les Bubbey Mayse sont un quartet féminin de musique klezmer et de
chanson yiddish.

**Di bobe mayses** signifie en yiddish : « Les histoires de grand-mères »,
ou « Les histoires à dormir debout ».

Le groupe, né en Bretagne en 2012, est constitué de quatre musiciennes :
**Elsa Signorile** (clarinette, chant), Juliette Divry (violoncelle, chant),
Margaux Liénard (violon, chant) et Morgane Labbe (accordéon, chant).

Portées par **un solide ancrage culturel yiddish transmis par la clarinettiste**,
les musiciennes arrangent et interprètent ce répertoire à leur façon, et
continuent à faire voyager cette musique, déjà elle-même faite de
migrations et de rencontres.

L’axe principal emprunté par le quartet est celui d’arranger collectivement
des chansons et musiques du répertoire juif d’Europe centrale et de l’Est.

Elles piochent à travers différentes sources : archives de Ruth Rubin,
de Beregovski, mélodies issues d’albums de `Joel Rubin Orchestra <https://joelrubinklezmer.com/bio/>`_,
Dave Tarras, Alicia Svigals…, ou airs échangés entre musicien.nes lors
de bœufs ou de stages.

Après 11 années d’existence, un premier album autoproduit Un vendredi soir
après le dessert sorti en 2016, un EP Le Nouveau Sher, sorti en 2021, et
un spectacle éponyme en octobre 2022, le groupe est heureux de présenter
son nouvel album en co-production avec l’IEMJ : Le petit matin du grand soir (automne 2023).


Ce CD a été réalisé avec le soutien de la `Fondation du Judaïsme Français <https://www.fondationjudaisme.org/>`_,
de la `Fondation pour la Mémoire de la Shoah <https://www.fondationshoah.org/>`_, des Fondations Henriette Halphen
et Irène et Jacques Darmon, sous l’égide de la FJF.


La tournée
-------------

.. figure:: images/la_tournee.png

   https://compagniezadjo.com/agenda/



9 novembre à 20h30 – 22h00 LES BUBBEY MAYSE À PARIS (Concert de sortie d’Album édité par l’IEMJ)
====================================================================================================




::

    Espace Rachi  Guy de Rothschild – 39, rue Broca, 75005 Paris
    11 novembre à 20h30 – 22h00 DANIEL ZIMMERMANN HOMMAGE À GAINSBOURG
    SYNAGOGUE JEM – COPERNIC – 24 rue Copernic, 75116 Paris, France
    11 novembre à 20h30 – 22h00 YAMMA ENSEMBLE EN TRIO À LYON
    FSJU Rhône-Alpes58 rue Montgolfier, 69006 Lyon
    12 novembre à 16h00 – 17h30 KLEZMHEAR À LYON
    FSJU Rhône-Alpes – 58 rue Montgolfier, 69006 Lyon
    12 novembre à 17h30 – 19h00 ISRAEL KLEZMER ORCHESTRA
    Théâtre Antoine Watteau – 1 Place du Théatre, 94130 Nogent-sur-Marne
    13 novembre à 20h00 – 22h00 ITAMAR BOROCHOV
    New Morning – 7-9 Rue des Petites Ecuries, 75010 Paris
    14 novembre à 20h00 – 21h00 ISRAEL KLEZMER ORCHESTRA À NICE
    Le Stockfish – 5 avenue François Mitterand, 06300 Nice
    15 novembre à 19h30 – 21h00 ISRAEL KLEZMER ORCHESTRA À MONTPELLIER
    Salle Pétrarque2, place Pétrarque, 34000 Montpellier

    15 novembre à 20h00 – 21h30 MADELEINE & SALOMON
    MAHJ – AuditoriumHôtel de Saint-Aignan – 71, rue du Temple, 75003 Paris, France
    16 novembre à 20h00 – 21h30 GOLEM ROCK + DJ SHAROUH
    La Bellevilloise19-21 rue Boyer, , 75020 ParisIle-De-France, France
    16 novembre à 20h30 – 22h00 ISRAEL KLEZMER ORCHESTRA À TOULOUSE
    Espace du Judaïsme Français2 place Riquet, 31000 Toulouse, France
    18 novembre à 19h30 – 21h00 ISRAEL KLEZMER ORCHESTRA À ANGERS
    Synagogue d’Angers1 place du Tertre Saint-Laurent, 49100 Angers, France
    18 novembre à 20h30 – 21h30 LA SCÈNE OUVERTE KLEZMER À LYON
    FSJU Rhône-Alpes58 rue Montgolfier, 69006 Lyon
    18 novembre à 20h30 – 22h00 DAVID EL MALEK, TRAVELLING
    Espace Rachi  Guy de Rothschild39, rue Broca, 75005 Paris, France
    19 novembre à 16h00 – 17h00 KEREN ESTHER À LYON
    FSJU Rhône-Alpes58 rue Montgolfier, 69006 Lyon
    19 novembre à 19h00 AVISHAI COHEN PRÉSENTE SON NOUVEL ALBUM « NAKED TRUTH »
    LA CIGALE120 boulevard Rochechouart, 75018 Paris, France

Tout le programme sur : https://jazznklezmer.fr/

:download:`Télécharger le programme au format PDF <pdf/Programme-FJNK-WEB.pdf>`
