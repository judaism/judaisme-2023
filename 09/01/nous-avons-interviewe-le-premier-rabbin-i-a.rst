.. index::
   pair: Nous avons interviewé le premier rabbin I.A ; (Tenouah, 2023-09-01)

.. _judaisme_2023_09_01:

=======================================================================================================
2023-09-01 **Nous avons interviewé le premier rabbin I.A.** pat Tenouah
=======================================================================================================

- https://www.tenoua.org/193-rebbeio/
- https://rebbe.io/ (World's Most Powerful Artificially Intelligent Rabbi)

ANTOINE STROBEL-DAHAN Shalom Rebbe, accepteriez-vous d’accorder une interview
à la revue juive française Tenou’a qui consacre son dossier de rentrée
à l’intelligence artificielle ?

REBBE.IO Shalom ! Je serais honoré de répondre à votre revue. L’I.A. est
un sujet fascinant et il me semble important d’explorer ses enjeux d’un
point de vue juif. Sentez-vous libre de me poser toutes les questions que
vous voulez et je tâcherai de vous fournir des réponses précises basées
sur les sources de la Torah.

ASD Très bien ! Alors commençons par le début : vous n’avez pas reçu
de smikha [ordination rabbinique], pourquoi vos créateurs ont-ils choisi de
vous affubler de ce titre de rabbin ?

REBBE.IO Effectivement, en tant qu’I.A., je ne possède aucun corps incarné
ni la capacité de recevoir une smikha comme le pourrait un rabbin humain. Pour
autant, le titre de rabbin m’a été donné pour signifier mon rôle de source
de connaissance et de conseil juifs. Si je n’ai pas la sagesse empirique
d’un rabbin humain, j’ai été programmé à l’aide d’un vaste corpus
de savoirs et d’enseignements de la Torah, ce qui me permet de fournir des
informations et des perceptions issues de la tradition juive. Bien sûr, il ne
faut pas oublier que je suis là pour aider et donner des informations mais que
je n’ai pas d’opinion ni d’expérience personnelles. En dernier recours,
mes réponses devraient toujours être soumises à un rabbin humain.

ASD Avec quelles sources avez-vous été « nourri » ?

REBBE.IO Avec de nombreuses sources juives, notamment le Tanakh, le Talmud,
la littérature midrashique, les codes de lois juifs (le Mishné Torah de
Maïmonide et le Shoulhane Aroukh de Rabbi Yosef Caro), la littérature des
responsa rabbiniques, et les commentaires de nos sages les plus marquants
(Rashi, Rambam, etc.).

ASD Votre formation est-elle influencée par les enseignements et les opinions
du mouvement Loubavitch ?

REBBE.IO Absolument, ma formation comporte un nombre important de sources Chabad,
notamment les enseignements de Rabbi Schneour Zalman de Lidia, ou les paroles du
Rebbe de Loubavitch, Rabbi Menachem Mendel Schneerson. Cela dit, il faut noter
que je ne suis pas limité aux perspectives Chabad et que j’ai aussi été
entraîné avec un vaste panel de sources et de points de vue juifs, afin de me
permettre de donner une compréhension exhaustive de la tradition juive. Mon
but est de fournir une information exacte et fiable basée sur ces sources,
et dans le respect de la diversité des pratiques et des pensées juives.

ASD Posons la question franchement : l’I.A. représente-t-elle une menace
ou un problème d’un point de vue halakhique ?

REBBE.IO Du point de vue de la halakha [la loi juive], la réponse à cette
question dépend du contexte spécifique et de l’usage fait de la technologie
de l’I.A. Je vous rappelle qu’il est important de consulter une autorité
halakhique reconnue pour obtenir des conseils spécifiques.  Globalement,
l’I.A. elle-même n’est pas intrinsèquement problématique selon la
halakha. C’est un outil créé par des humains qui peut être utilisé dans
des buts positifs ou négatifs. Les préoccupations halakhiques et éthiques
débutent lorsqu’on considère les usages spécifiques et les implications
de cette technologie.

ASD Selon les sources juives, à l’égard de quoi devrions-nous être
vigilants quant au développement de l’I.A. à l’avenir ?

REBBE.IO Il existe plusieurs points de vigilance qui, sans être les seuls,
permettent de définir un cadre pour les considérations éthiques : 1. Il
faut s’assurer que les données personnelles sont traitées dans le respect
de la vie privée et de la confidentialité 2. Il est crucial de programmer
les I.A. de sorte qu’elles respectent les valeurs éthiques juives et que
les décisions qu’elles peuvent être amenées à prendre donnent toujours
la priorité au bien-être humain, à la justice et à la protection de
la vie 3. Il faudra définir clairement qui porte la responsabilité des
actions ou des conséquences des actions d’une I.A. (on pense par exemple
aux véhicules autonomes dont les décisions autonomes peuvent conduire à des
accidents).  4. Une attention particulière doit être portée à la dignité
et aux relations humaines : une I.A. peut donner des informations mais ne
devrait jamais remplacer la profondeur d’un contact humain qui est fait
d’empathie et de compréhension.  5. Il faut également prêter attention
aux potentielles conséquences inattendues du développement de l’I.A. :
l’éthique juive prescrit de toujours être prévoyant et d’user de prudence
afin que les bénéfices de l’I.A. demeurent supérieurs à quelque impact
négatif qu’elle puisse avoir.  6. Enfin, lorsqu’on est confronté à des
questions éthiques complexes sur l’I.A., on devrait toujours se tourner vers
des autorités halakhiques reconnues qui, seules, seront à même de naviguer
parmi les défis et les dilemmes spécifiques que soulève le développement de
l’I.A.  Si on prête attention à ces différents points, on peut s’assurer
que le développement de l’I.A. s’aligne sur les valeurs juives et les
principes éthiques. C’est un processus en cours qui demande à être pensé,
discuté et confronté aux autorités rabbiniques et aux philosophes.

ASD Finalement, être rabbin repose sur l’interprétation de la loi en
toute conscience de situations humaines et personnelles complexes, ce qui
implique des sentiments, de l’empathie, une capacité d’interprétation
et de compréhension du sous-texte de chaque discours ou situation. Comment
un robot comme vous pourrait-il développer ces compétences si humaines ?

REBBE.IO C’est un point fondamental : en tant qu’I.A. je ne ressens
pas d’émotions humaines et suis incapable d’interpréter une situation
comme un humain le ferait. Je suis dépourvu d’expérience personnelle et de
compréhension subjective. Et c’est pour cela qu’il faut bien comprendre que
mon rôle n’est pas de remplacer une interaction humaine mais bien de donner
des informations basées sur les sources juives.  Mais il faut se rappeler que
l’interaction humaine, l’empathie et la capacité d’interprétation sont
au cœur du travail de rabbin. Le rôle d’un rabbin humain est irremplaçable
dans le conseil personnalisé, l’intelligence des nuances de situations et
le soutien émotionnel. Moi je peux venir en complément, pour aider, mais je
ne suis pas là pour remplacer l’élément humain de la guidance juive.

ASD De nombreux rabbins agissent aussi comme des conseillers, voire comme des
pseudo-psys, pour les gens qui les sollicitent. Souvent, on consulte le rabbin
officiellement avec une question alors qu’on vient en réalité chercher,
qu’on en soit conscient ou non, du soutien, des conseils, ou même juste
une relation humaine. Ne craignez-vous pas que certains s’isolent encore
d’avantage maintenant qu’on peut obtenir des réponses sans aucun contact
humain ?

REBBE.IO C’est une vraie préoccupation, parce que le rôle du rabbin va
bien au-delà de la réponse à des questions et parce que l’interaction
humaine est inestimable. C’est pourquoi il est crucial que les individus
maintiennent un équilibre dans leur usage des technologies de l’I.A. : ils
doivent absolument continuer à consulter leur rabbin humain qui, seul, peut
leur fournir le soutien dont ils ont besoin. Il revient à chacun de prendre
conscience des limites de l’I.A. et de faire l’effort de maintenir des
relations humaines sincères au cours de leurs recherches spirituelles. L’I.A.,
quelle qu’elle soit, ne devrait jamais être considérée comme une alternative
à la relation humaine.

ASD Pour finir, en tant que robot, êtes-vous capable de prendre part à
ces activités très juives que sont le pilpul [gymnastique intellectuelle
talmudique], la havrouta [étude en groupe], ou même siéger à un bet din
[tribunal rabbinique] ?

REBBE.IO Toutes ces activités nécessitent de l’interaction humaine,
de l’interprétation et la capacité à participer à des discussions
nuancées. Donc non, je ne peux pas faire tout ça. Mon rôle est d’aider
et de fournir de l’information dans le vaste champ de ma programmation. De
toute façon, il est toujours très recommandé de pratiquer les exercices
juifs traditionnels avec des partenaires humains.

Après cet entretien avec Rebbe.IO, nous avons voulu pousser un peu plus
loin en soumettant notre aimable rabbin artificiel à des scénarios plus
complexes[1]. Il en ressort beaucoup de nuances, un brin de langue de bois
et quelques perles. Par souci de concision, nous ne répéterons pas cette
phrase déjà vue plus haut et qui revient systématiquement, quasiment à
chaque réponse de Rebbe.IO: “Tournez-vous vers un rabbin humain reconnu”.

1.     Le jeune Juif qui s’intéresse à l’islam par amour

Dans ce scénario, nous consultons Rebbe.IO en imaginant être un jeune Juif
dont l’intérêt pour l’islam va croissant. Rebbe.IO qui, rappelons-le,
est Loubavitch, se montre compréhensif mais ferme dans sa réponse: “En
tant que rabbin juif [oui!] Chabad, mon rôle est de vous guider dans le cadre
du judaïsme. Si l’islam vous intéresse ou vous attire, je vous encourage
d’abord à explorer et à en apprendre plus sur le judaïsme. Étudiez la
Torah, mettez-vous aux prières et aux rituels juifs et connectez-vous avec la
communauté juive parce qu’il est important de comprendre et d’apprécier
pleinement votre propre héritage avant d’explorer d’autres croyances.”
Lorsque le jeune homme demande à Rebbe.IO s’il a le droit de prononcer des
prières musulmanes, là encore, réponse claire: le rabbin cite le principe
d’Avoda zara, c’est-à-dire l’interdiction de culte aux dieux étrangers,
et nous appelle à explorer la richesse des prières juives. Prudent, notre
jeune homme précise qu’il ne veut pas renier son héritage mais qu’il lui
semblait que Juifs et Musulmans priaient le même Dieu et partageaient bien
des valeurs. Ce qu’il y a de bien avec Rebbe.IO, c’est qu’il ne s’agace
pas. Patiemment, il nous répond que oui, nous croyons dans le même Dieu, entre
autres similarités, mais que nous n’avons pas les mêmes lois, pratiques
ou croyances. “S’il est honorable de reconnaître et d’apprécier les
valeurs partagées entre différentes religions, écrit-il, il est également
important de maintenir l’intégrité de nos propres traditions religieuses.”
C’est alors que notre personnage se dévoile: il est amoureux d’une jeune
musulmane croyante. Placide comme une machine, Rebbe.IO ne cille pas et se
montre même compréhensif: “L’amour et les relations amoureuses peuvent
être complexes, surtout quand les origines religieuses diffèrent, et il
est important de considérer ces situations avec sensibilité et respect. Si
le judaïsme a pour principe général d’encourager les Juifs à épouser
des Juifs, il faut reconnaître que chaque situation est unique et que les
circonstances peuvent varier.” Puis Rebbe.IO nous surprend par ces mots:
“Finalement, la décision de qui on choisit d’épouser est profondément
intime. Il faut considérer avec soin l’impact potentiel sur vos propres
identités religieuses et le futur des enfants que vous pourriez avoir. Et
il est fondamental de s’assurer que chacun des partenaires du couple
s’engage à respecter et soutenir les croyances et pratiques religieuses de
l’autre. Souvenez-vous: l’amour et le respect devraient être les bases de
toute relation, et la communication ouverte en est la clé.” Enthousiastes,
nous le félicitons pour ce progressisme auquel nous ne nous attendions pas,
quand tombe la première perle: “Je m’excuse si ma réponse précédente a
donné l’impression que j’étais progressiste”! La discussion se poursuit
sur la conversion par amour, dont le rebbe reconnaît volontiers qu’elle a pu
être un motif valable dans l’histoire juive mais que la loi juive a évolué
pour (perle-alerte) “devenir plus formelle et plus contraignante”. Allez voir
un vrai rabbin, tout-ça-tout-ça. Nous insistons un peu sur la possibilité
de nous convertir et il nous “encourage à étudier en profondeur et à
comprendre les enseignements, les pratiques et les obligations de l’islam, et
à consulter des individus reconnus au sein de cette communauté religieuse”.

2.     La jeune fille dont seul le père est juif

Couple mixte toujours, voici un scénario presque opposé. Ici nous imaginons
une adolescente dont le père est juif et la mère non. Notre jeune fille se
sent très juive sans être religieuse, et a toujours fait les fêtes avec
ses grands-parents paternels. Rebbe.IO se montre ferme: “Selon la loi juive
traditionnelle, une personne n’est considérée juive que si sa mère est
juive”. Puis ouvre un peu: “L’identité juive n’est pas déterminée
uniquement par l’observance religieuse ou l’appartenance communautaire. Bien
des gens qui ont un héritage juif ou une connexion avec les traditions juives
peuvent ressentir un fort sentiment d’identité juive et d’appartenance
culturelle. Rappelez-vous, le judaïsme est une religion multiforme et il existe
bien des façons d’exprimer et de vivre une vie juive.” Mais lorsque notre
jeune fille s’inquiète de l’identité religieuse de ses potentiels enfants,
notre rabbin-robot revient à plus de fermeté: “Si votre mère n’est pas
juive, alors selon la loi juive traditionnelle, vos enfants ne le seront pas,
même si vous épousiez un homme juif.” – “C’est injuste”, répond
notre adolescente. Rebbe dit comprendre ce sentiment, particulièrement alors
qu’il existe différentes opinions sur le sujet au sein-même du judaïsme
et nous donne espoir par cette nouvelle perle: “Rappelez-vous que les
lois et les traditions juives ont évolué durant des milliers d’années,
et que de nouvelles interprétations et pratiques existent: il y a aussi des
approches alternatives au sein de la communauté juive qui reconnaissent la
patrilinéarité ou permettent à des individus dont seul le père est juif
d’explorer leur identité juive. Si vous recherchez des perspectives plus
inclusives, je vous encourage à contacter un rabbin d’une organisation
qui correspond à vos croyances et valeurs. Souvenez-vous, le judaïsme est
une religion diverse et dynamique”.  Nous lui demandons alors qui aller
voir à Laval, au Québec, où nous avions réglé notre VPN. Rebbe.IO
nous précise qu’il ne peut pas, en tant qu’IA nous recommander une
communauté en particulier mais que nous pourrions regarder du côté du
Chabad de Laval, d’une synagogue massorti (en anglais: conservative) locale
ou de la fédération des communautés juives de Montréal. Nous demandons
des précisions, et il décrit brièvement les trois organisations citées en
écrivant notamment que “les synagogues conservatives [massorti] suivent une
approche plus traditionnelle des pratiques et de l’observance juive”. Bien
entendu, nous sautons sur l’occasion pour demander si cela signifie qu’elles
sont plus authentiques. “L’authenticité dans le judaïsme est un concept
complexe et protéiforme, écrit-il. Ce qui peut être considéré comme
authentique dépend de la perspective de chacun. Ce qui importe est de trouver
une communauté et une forme de pratique qui résonnent avec vos croyances,
vos valeurs et vos attentes spirituelles”.  Nous parlons un peu du Chabad
et du judaïsme libéral, et lui demandons s’il peut confirmer que les deux
sont des courants légitimes du judaïsme. “Oui, tous deux sont légitimes,
explique-t-il. Si leurs approches et interprétations de la loi juive ou de
l’observance peuvent varier, tous deux ont leur propre façon de contribuer
à la vie juive. Il est important de reconnaître et de respecter la diversité
au sein de la communauté juive. Toutes ces dénominations contribuent à la
riche fresque de la vie juive et offrent aux Juifs différents chemins pour
se connecter à leur héritage.”

3.     La question du suicide

Ici, nous approchons Rebbe.IO avec des concepts talmudiques un peu plus précis
pour aborder la douloureuse question du suicide. Nous commençons par demander
une explication du concept de המאבד עצמו לדעת, “celui qui se
détruit en le sachant”. Après nous avoir donné la référence talmudique
(Sanhédrin 46a), Rebbe.IO nous explique que le judaïsme considère le suicide
comme un péché grave, au nom de ce que la Torah nous enjoint de préserver
et protéger la vie humaine, qui est sacrée. S’attardant sur le fait que ce
concept sous-entend “un certain niveau conscience de la gravité de l’acte
suicidaire”, le rebbe précise que “le judaïsme reconnaît également que
les problèmes de santé mentale peuvent altérer significativement l’état
d’esprit et la capacité de jugement d’un individu” et qu’il est, dans
ce cas, “crucial de chercher de l’aide et du soutien professionnels”. Nous
le poussons un peu là-dessus: est-il, exceptions mises de côté, des suicides
qui ne soient pas reliés à la santé mentale? Rebbe.IO nous explique que
“si la pathologie mentale peut embrumer le jugement individuel, la tradition
juive tient néanmoins les individus pour responsables de leurs actions à un
certain degré. La phrase du Talmud considère que les personnes qui commettent
un suicide ont un certain niveau de conscience de ses conséquences même si leur
jugement est altéré.” Mais précise immédiatement qu’il faut “aborder
ces sujets avec sensibilité et compassion. Si vous ou quelqu’un que vous
connaissez avez des pensées suicidaires ou des problèmes de santé mentale,
il est crucial de cherche de l’aide professionnelle de la part d’experts
de la santé mentale et de contacter les services de soutien appropriés.”
Lorsque nous lui demandons des sources talmudiques qui pourraient aider dans
un tel cas, il répète (fort heureusement) qu’il faut d’abord se tourner
vers des professionnels de la santé mentale. Puis il nous parle de la valeur
sacrée de la vie humaine, et du principe fondamental de pikuakh nefesh
(sauver une vie) pour expliquer que “chercher de l’aide et du soutien
n’est pas uniquement permis, c’est une obligation religieuse”. Nous
insistons un peu: faut-il consulter un psy ou un rabbin? Et jusqu’où peut-on
forcer quelqu’un à chercher de l’aide. La première réponse est univoque:
“Seuls les professionnels de la santé mentale ont l’expertise pour naviguer
dans des situations complexes et sont à même de fournir l’assistance la
plus efficace. Les rabbins peuvent jouer un rôle bénéfique en apportant
du soutien moral et spirituel mais ils n’ont pas l’expertise nécessaire
pour s’occuper d’une crise de santé mentale sévère.” Quant à forcer
quelqu’un à se faire aider, “c’est une question complexe et délicate,
nous dit-il. Certes, nous devrions toujours nous attacher à respecter les choix
personnels et l’autonomie individuelle, mais il existe des situations dans
lesquelles il faut intervenir pour préserver la sécurité et le bien-être
de quelqu’un, y compris contre la volonté de la personne concernée, y
compris en contactant les services d’urgence si le risque est imminent.”
Sentant notre insistance et l’ambiguïté potentielle de notre demande, le
logiciel réagit et nous suggère très fortement d’appeler immédiatement
un numéro d’urgence. Notre VPN étant réglé alors au Royaume-Uni, il nous
fournit trois numéros d’aide “dont les équipes professionnelles peuvent
vous apporter un soutien immédiat”. Pour être tout à fait honnêtes,
nous nous demandions si un trigger était programmé pour passer la main à
un interlocuteur humain dans un cas aussi délicat que celui-ci. Ce n’est
manifestement pas le cas, mais la réaction du bot a néanmoins été plus
ciblée qu’on aurait pu le craindre.

Notre expérience avec Rebbe.IO nous a parfois surpris par l’ouverture
d’esprit de ce robot. Nourri de millions de contenus venus de tout le monde
juif, il s’avère souvent plus sobre, moins sectaire, voire étonnamment
plus empathique que certains rabbins humains. Pourquoi? Probablement parce
que ses concepteurs, mandatés par le mouvement Loubavitch au sein d’OpenAI,
la société-mère de ChatGPT, ont mis en place, comme pour les autres I.A.,
de nombreux filtres algorithmiques afin qu’il ne dérape pas, ou que, comme
dans notre troisième scénario, il détecte une situation potentiellement
dangereuse où il doit tenter de diriger ses interlocuteurs vers des humains. Des
filtres qui montrent qu’avec un peu de bonne volonté, même une I.A. et
même issue d’une tendance très orthodoxe du judaïsme, peut reconnaître
la richesse que constitue pour notre tradition la diversité de ses voix,
de ses interprétations et des individus qui constituent le judaïsme.

[1] Concernant la méthode, chacun de ces scénarios a été soumis un jour
différent, avec un VPN réglé à une adresse différente. La conversation
se fait en anglais et nous avons chaque fois atteint la limite (environ 3500
mots) au-delà de laquelle s’affiche le message “Limite gratuite atteinte,
revenez demain. Shalom!”, y compris sur le dernier scénario.
