.. index::
   pair: Célébrer l'égalité et le mois naissant ; Floriane Chinsky
   pair: Women of the Wall ; Femmes du mur


.. _celebrer_egalite_2023_09:
.. _wow_5784:

===================================================================================================================================
**Célébrer l'égalité et le mois naissant/Femmes du mur (Women of the Wall)** par rabbin Floriane Chinsky à Surmelin et en ligne
===================================================================================================================================

- https://rabbinchinsky.fr/cours-5784/
- https://rabbinchinsky.fr/offices-wow-5784/
- https://womenofthewall.org.il/rosh-hodesh/

Pour vous inscrire, c’est ici : https://framaforms.org/inscription-offices-femmes-du-mur-1695899101

:download:`Télécharger les cours de Floriane Chinsky 2023-2024 au format pdf <../pdfs/cours-floriane-chinsky-2023-2024-kippour.pdf>`

- https//womenofthewall.org.il
- https://www.youtube.com/@FlorianeChinsky/videos

.. figure:: images/floriane-chinsky-celebrer-l-egalite-et-le-mois-naissant.png
   :align: center


A coller sur votre frigo/A partager ! Cours du Rabbin ! Prenez date !

Rabbin Floriane Chinsky

**A Surmelin et en ligne**

**Le Thème : Roch Hodech**
==============================

- https://womenofthewall.org.il/rosh-hodesh/

.. figure:: images/wow_rosh_hodech.png
   :align: center



Le Thème : Roch Hodech est la fête du premier du mois juif.

**Elle donne lieu à des festivités oubliées.
La place des femmes est au cœur de ces célébrations**.

Les Facilitantes
------------------

- La Rabba Floriane Chinsky a poursuivi ses études rabbiniques en Israël,
  elle conduit un office en ligne avec étude de décryptage.

- Yaëlle Tordimann est cheffe de chœur. Elle anime la Chorale Zimsat Ya
  dans le cadre des ateliers Garin.

Samedi précédant Roch Hodech : Annonce du nouveau mois à l'office, et
surtout CHORALE ZIMRAT YA de 14h à 15h à Surmelin.

Matin de Roch Hodech : Office du matin de 6h à 7h15, avec suivi en direct de
l'office des femmes du mur et partages entre nous : chants, parties
essentielles de l'office, partage, étude concernant la place des femmes.

En ligne : Vidéo générale sur la chaine youtube de Judaïsme en Mouvement,

Vidéo d'étude approfondie du texte sur la chaine `youtube Floriane Chinsky <https://www.youtube.com/@FlorianeChinsky/videos>`_

- Rosh Hodesh Heshvan — Monday, October 16, 2023
- Rosh Hodesh Kislev - Tuesday, November 14, 2023
- Rosh Hodesh Tevet - Wednesdey, December 13, 2023
- Rosh Hodesh Shevat — Thursday, January 11, 2024
- Rosh Hodesh Adar 1— Friday, February, 9, 2024
- Rosh Hodesh Adar 1 — Monday, March 11, 2024
- Rosh Hodesh Nisan — Tuesday, April 9, 2024
- Rosh Hodesh Ivar — Wednesday, May 8, 2024
- Rosh Hodesh Sivan — Friday, June 7, 2024
- Rosh Hodesh Tammuz — Sunday, July 7, 2024
- Rosh Hodesh Av- Monday, August 5, 2024
- Rosh Hodesh Elul- Tuesday, September 3, 2024

Inscriptions et dernières informations pour tous les cours:

Abonnez-vous au site rabbinchinsky.fr, au facebook de women of the wall,
visitez https//womenofthewall.org.il
