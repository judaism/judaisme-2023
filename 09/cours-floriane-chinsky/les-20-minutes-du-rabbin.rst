.. index::
   pair: Les 20 minutes du rabbin ; Floriane Chinsky


.. _floriane_chinky_20_minutes_2023:

======================================================================================================================
**20 minutes de la Rabbin 5784, à Surmelin, Samedi matin une fois par mois, 10h-10h20**
======================================================================================================================

- https://rabbinchinsky.fr/cours-5784/
- https://rabbinchinsky.fr/20-minutes-5784/


:download:`Télécharger les cours de Floriane Chinsky 2023-2024 au format pdf <../pdfs/cours-floriane-chinsky-2023-2024-kippour.pdf>`

.. figure:: images/floriane-chinsky-les-20-minutes-du-rabbin.png
   :align: center

A coller sur votre frigo/A partager ! Cours du Rabbin ! Prenez date !

**Surmelin, Samedi matin une fois par mois, 10h-10h20**

**En présentiel**, les documents sources seront partagés en ligne à travers
la newsletter du site Rabbinchinsky.fr

Le Thème : Que dit le judaïsme de ce qui fait notre quotidien ?
=====================================================================

Qui sommes-nous, quelles sont nos relations aux autres, comment vivons-nous
dans la société ?

Ces questions concernent les adultes et les adultes en devenir, de
10 à 110 ans.

20 minutes est un format court, qui permet de soulever certaines questions
à rediscuter en famille et entre amis.

À chaque fois, **Sarah Berrebi** du Pôle Jeunesse de JEM sera présente à
l'office qui suit et emmènera les plus jeunes poursuivre le débat autour
de boissons et de gâteaux.

La Facilitante
-----------------

La Rabba Floriane Chinsky est au croisement du judaïsme, de la sociologie
et de la pédagogie.

1. 14 octobre 2023 - Que signifie Être Adulte dans le judaïsme ?
2. 18 novembre 2023 - Que signifie Être Juif ou Juive ?
3. 16 décembre - Comment être solidaires ?
4. 13 janvier Le judaïsme, l'écologie, notre avenir
5. 27 janvier - L'amitié
6. 2 mars 2024 - Les conflits : pardonner, confronter, que dit le judaïsme ?
7. 16 mars 2024 — La sexualité, un sujet tabou ?
8. 27 avril — Tailleur pour hommes ou Tailleur pour femme ?
   Médecin ou avocate ? Le travail selon le judaïsme
9. 25 mai 2024 - Les agressions identitaires, antisémitisme, sexisme et autres.
10. 22 juin 2024 — Le pouvoir, un objectif ou un tabou ? Qu'en dit le judaïsme.


Inscriptions et dernières informations pour tous les cours
Abonnez-vous au site rabbinchinsky.fr

