.. index::
   pair: Café ; Café des psaumes
   pair: Leadership inclusif dans la pensée juive ; Floriane Chinsky


.. _leadership_inclusif_2023:

=====================================================================================================================================================================================================
**Leadership inclusif dans la pensée juive** par Floriane Chinsky **Café des Psaumes** 3° mercredi du mois, 19h-21h30 première partie accessible par zoom, deuxième partie uniquement en présentiel
=====================================================================================================================================================================================================

- https://rabbinchinsky.fr/cours-5784/
- https://rabbinchinsky.fr/leadership-5784/
- https://www.cafedespsaumes.org/le-cafe-des-psaumes

:download:`Télécharger les cours de Floriane Chinsky 2023-2024 au format pdf <../pdfs/cours-floriane-chinsky-2023-2024-kippour.pdf>`


.. figure:: images/cafe_des_psaumes..png
   :align: center


.. figure:: images/floriane-chinsky-leadership-inclusif.png
   :align: center


.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.357924580574036%2C48.85624815594721%2C2.3614650964736943%2C48.857682885854715&amp;layer=tracestracktopo"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/48.85697/2.35969&amp;layers=P">Afficher une carte plus grande</a></small>



A coller sur votre frigo/A partager | Cours du Rabbin ! Prenez date

**Café des Psaumes, 3° mercredi du mois, 19h-21h30**

Première partie accessible par zoom, deuxième partie uniquement en présentiel


Le Thème : La structuration hiérarchique semble avoir triomphé en France
================================================================================


Correspond-elle à la vision juive ?
Dieu serait-il au sommet de la pyramide ?
La prière serait-elle une imploration ?
La royauté serait-elle au centre ?
Le judaïsme a-t-il un dogme imposé, une loi coercitive ?
Les rabbins font-iels partie de cette hiérarchie ?
Les dimensions dites masculines sont-elles dominantes ?
Les hommes y sont-ils mieux placés que les femmes ?

À chaque rencontre, un exposé et une mise en situation, pour faire rentrer
nos idées et nos espérances dans notre quotidien.

La Facilitante : Notre conférencière, Docteure en Sociologie du Droit,
Rabbin en exercice et Formatrice en Ecoute Mutuelle exposera les grands
axes du judaïsme, de l'antiquité à nos jours, en mettant en avant les
dimensions d'intelligence collective développées au fil des générations.

**Elle partagera des outils concrets pour vivre la puissance non oppressive
du collectif au quotidien**.

1. 18 octobre 2023 — Le leadership divin comme leadership de la largesse et de l'inclusion
2. 15 novembre 2023 - De la prêtrise standardisée à la pensée libérée, le talmud
3. 20 decembre 2023 — Le sacré dans le quotidien, se faire du bien pour rester libre
4. 17 janvier 2024 La loi comme loi d'amour et de structuration
5. 20 mars 2024 — Le cœur comme moteur, tefila personnelle et écoute mutuelle
6. 15 mai 2024 - Le collectif comme catalyseur, tefila collective et vision non hiérarchique
7. 19 juin 2024 — Conclusion de l’année

Inscriptions et dernières informations pour tous les cours Abonnez-vous au site rabbinchinsky.fr


