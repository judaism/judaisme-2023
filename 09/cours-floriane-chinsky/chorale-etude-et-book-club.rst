.. index::
   pair: Chorale Etude et Book Club Ateliers ; Floriane Chinsky



====================================================================================================
**Chorale Etude et Book Club Ateliers Garin/Rabbin Floriane Chinsky** Surmelin, le Chabat matin
====================================================================================================

- https://rabbinchinsky.fr/about/agenda/

:download:`Télécharger les cours de Floriane Chinsky 2023-2024 au format pdf <pdfs/cours-floriane-chinsky-2023-2024-kippour.pdf>`


.. figure:: images/floriane-chinsky-book-club.png
   :align: center

Ateliers Garin/ Rabbin Floriane Chinsky
Surmelin, le Chabat matin,
9h15 (Chabateliers et café), 10h (20 minutes)

- 13h (paracha ou texte du rabbin)
- et 14h (Chorale ou book-club}

En présentiel, les documents sources seront partagés en ligne à travers
la newsletter du site Rabbinchinsky.fr

Le Thème : Chabat
======================

Le Thème : Chabat est le moment du rassemblement communautaire

Chacun.e est invitée à passer un moment de rencontre, Le chant, l'étude, la
discussion, les ateliers de transmission du savoir, sont autant d'occasions
d'avancer dans nos vies.

Les facilitant.es : Autour de notre rabbin, un petit groupe de personnes met
en place des activités.

Chorale Zimrat ya
======================

Chorale Zimrat ya, Yael Tordimann et le groupe Garin, tous les chabat
d'annonce du nouveau mois, de 14h à 15h

- Dates:

  - 09/09/2023,
  - 14/10/2023,
  - 11/11/2023,
  - 09/12/2023,
  - 03/02/2024,
  - 06/04/2024,
  - 09/04/2024,
  - 04/05/2024,
  - 01/06/2024,

Notre paracha, Charles et le groupe Garin
===============================================

Notre paracha, Charles et le groupe Garin, étude interactive de la paracha,
le chabat de 13h, 14h

- Dates :

  - 07/10/2023,
  - 11/11/2023,
  - 16/12/2023,
  - 27/01/2024,
  - 02/03/2024,
  - 27/04/2024,
  - 22/06/2024


Book Club, Elisabeth et le groupe Garin
-----------------------------------------

Book Club, Elisabeth et le groupe Garin, discussion de livres sur les
thèmes les plus centraux de l'identité juive, le chabat, de 14h à 15h

- Dates :

  - 04/11/2023,
  - 13/01/2024,
  - 16/03/2024,
  - 24/04/2024,
  - 29/06/2024

Chabateliers: café et ateliers
----------------------------------

Chabateliers: café et ateliers pour apprendre et réviser l'hébreu,
l'histoire juive, les personnages de la bible. le chabat matin de 9h15à 10h20

- Dates :

  - 14/10/2023,
  - 04/11/2023,
  - 11/11/2023,
  - 18/11/2023,
  - 09/12/2023,
  - 16/12/2023,
  - 13/01/2024,
  - 27/01/2024,
  - 03/02/2024,
  - 09/03/2024,
  - 16/03/2024,
  - 23/03/2024,
  - 27/04/2024,
  - 04/05/2024,
  - 25/05/2024,
  - 01/06/2024,
  - 22/06/2024

Inscriptions et dernières informations
---------------------------------------------

Inscriptions et dernières informations pour tous les cours
Abonnez-vous au site rabbinchinsky.fr et consultez
https://rabbinchinsky.fr/about/agenda/


