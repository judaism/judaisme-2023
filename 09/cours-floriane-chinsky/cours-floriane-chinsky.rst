.. index::
   pair: Cours ; Floriane Chinsky


.. _cours_floriane_chinsky_2023_09:
.. _cours_floriane_chinsky_5784:

=======================================================================================================
2023-09 **Cours de Floriane Chinsky 5784 (2023-2024 de l'ère commune)**
=======================================================================================================

- https://rabbinchinsky.fr/cours-5784/
- https://rabbinchinsky.fr/about/agenda/

:download:`Télécharger les cours de Floriane Chinsky 2023-2024 au format pdf <pdfs/cours-floriane-chinsky-2023-2024-kippour.pdf>`


.. figure:: images/floriane-chinsky-menu-5784.png
   :align: center

   https://rabbinchinsky.fr/cours-5784/

.. toctree::
   :maxdepth: 3

   les-20-minutes-du-rabbin
   leadership-inclusif-dans-la-pensee-juive
   chorale-etude-et-book-club
   celebrer-l-egalite-et-le-mois-naissant
