.. index::
   !Chofar

.. _chofar_2023_09:

=======================================================================================================
2023-09 **Les femmes et le chofar ... Cho far, Cho good**
=======================================================================================================

- https://www.etzion.org.il/he/halakha/orach-chaim/holidays/women-and-shofar-blowing

:download:`Télécharger Les femmes et le chofar ... Cho far, Cho good <pdfs/etude-chofar-femmes-du-mur.pdf>`



Mahari Seguel a enseigné : toute personne est tenu par le commandement
du Chofar, à la fois les mineurs et les adultes.

Les femmes en revanche sont exemptées, car c’est un commandement positif
lié au temps, mais elles se sont rendues tenues. Et du fait qu’elles se
sont rendues tenues, elles doivent se dépêcher de se préparer, se vêtir,
cuisiner, pour qu’elles soient libres d’aller à la synagogue et d’entendre
la sonnerie du chofar.

Elles ne doivent pas obliger la communauté à les attendre.

Il dit qu’en Autriche les femmes avaient l’habitude de cuisiner le jour
précédent, roch hachana, pour que à roch hachana elles puissent être libres
d’aller à la synagogue et après avoir quitté la synagogue, elles réchauffaient
la nourriture.
Elles doivent se préparer à être à la synagogue, à la fois les femmes et
les jeunes filles, pour entendre les offices et la sonnerie du chofar,
du début à la fin et telle est la pratique aujourd’hui.

Puisque les femmes ont pris pour elles-mêmes le commandement de la sonnerie
du chofar, il est approprié si c’est possible qu’elles laissent
leurs bébés à la maison, pour qu’ils n’interfèrent pas avec leur écoute
de la sonnerie du chofar, et si une femme n’est pas en mesure de laisser
son enfant à la maison, il est mieux de le garder avec elle dans la
section des femmes de la synagogue puisqu’elles ne sont pas tenues au
commandement comme les hommes.

Mais une femme qui laisse son enfant à la maison est digne de louange,
car elle s’est obligée elle-même, et on récite également la bénédiction
sur la sonnerie du chofar pour elles.
