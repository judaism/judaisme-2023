.. index::
   ! Judaïsme 2023


.. raw:: html

   <a rel="me" href="https://babka.social/@pvergain"></a>
   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://kolektiva.social/@noamsw"></a>


|FluxWeb| `RSS <https://judaism.gitlab.io/judaisme-2023/rss.xml>`_

.. _judaisme_2023:

=======================================================================================================
🕎 🕯️🌄🎶💐💗⚖  **Judaïsme 2023** 🕎🕯️ 🌄🎶💐💗⚖
=======================================================================================================

- https://fr.wikipedia.org/wiki/Portail:Juda%C3%AFsme
- https://www.jewfaq.org/index.shtml
- https://rstockm.github.io/mastowall/?hashtags=mazeldon,jewish,jewdiverse&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=torah&server=https://framapiaf.org

#ShalomSalaam #CeaseFireNow #Israel #Gaza #Mazeldon #Jewdiverse #Peace #Palestine
#jewish #ethics #morals #values #betterworld #israel #hebrew

.. toctree::
   :maxdepth: 4

   12/12
   11/11
   10/10
   09/09
   07/07
   06/06
   chodesh-tov/chodesh-tov
   festival-des-cultures-juives/festival-des-cultures-juives
   news/news
